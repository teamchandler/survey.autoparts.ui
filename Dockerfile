FROM octohost/nodejs-nginx
CMD 'bower install'
CMD 'grunt build'
WORKDIR /dist
ADD .  /var/www
CMD 'nginx'

