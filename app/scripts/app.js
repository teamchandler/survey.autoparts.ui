'use strict';

angular
  .module('loyalty.survey.ui', [
    'ui.router',
    'ui.bootstrap',
    'LocalStorageModule',
    'ngLodash',
    'underscore'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/login');

    $stateProvider

      .state('main', {
        url: '/main',
        templateUrl: 'views/main_base.html',
        controller: "MainBaseCtrl"
      })

      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: "LoginCtrl"
      })

      .state('main.call_details', {
        url: '/call_details',
        templateUrl: 'views/call_details.html',
        controller: "CallDetailsCtrl"
      })
      .state('main.retailer_details', {
        url: '/retailer_details',
        templateUrl: 'views/retailer_details.html',
        controller: "RetailerDetailsCtrl"
      })
      .state('main.feedback', {
        url: '/feedback',
        templateUrl: 'views/feedback.html',
        controller: "FeedBackCtrl"
      })

      .state('main.create_login_caller_user', {
        url: '/create_login_caller_user',
        templateUrl: 'views/create_login_caller_user.html',
        controller: "CreateCallerCtrl"
      })



      .state('main.created_feedback_question', {
        url: '/created_feedback_question',
        templateUrl: 'views/created_feedback_question.html',
        controller: "CreateFeedBackCtrl"
      })


  })

  .directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseInt(digits, 10);
          }
          return undefined;
        }

        ctrl.$parsers.push(inputValue);
      }
    };
  })
;
