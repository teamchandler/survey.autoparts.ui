/**
 * Created by Mahiruddin Seikh on 07-06-2016.
 */

'use strict';


angular.module('loyalty.survey.ui')
    .service('LoginService', function MasterService($rootScope,UtilService) {

//        Get user details for user  login
        this.user_login = function ($http, $q, credential) {

            var apiPath = UtilService.get_login_api() + '/user/login';
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: apiPath,
                data: credential,
                ContentType: 'application/json'
            }).success(function (data) {
                deferred.resolve(data);
            }).error(function (data) {
                deferred.reject("Data Error");
            });
            return deferred.promise;
        }
    });
