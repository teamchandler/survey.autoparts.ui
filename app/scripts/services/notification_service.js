'use strict';

angular.module('loyalty.survey.ui')

    .service('NotificationService', function NotificationService($rootScope) {

// ***************************************************************
//          Data Elements that will be shared across
// ***************************************************************
//        var new_product_list = "";
//        var min_price = 0;
//        var max_price = 0;
// ***************************************************************

// ***************************************************************
//          This is to centralize all notifcations used
//          in all the controllers
// ***************************************************************
        // This is used for sending broadcast message and data from rasing controller


        this.error_occured = function (broadcast_message, error_object, source, o) {
            this.error_object  = error_object;
            this.error_source = source;
            this.o = o;
            this.notify(broadcast_message)
        };
        this.change_header = function (broadcast_message, header_label) {
            this.header_label =  header_label;
            this.notify("change header")
        };
        this.send_notification = function (broadcast_message, notification_data) {
            this.notification_data =  notification_data;
            this.notify(broadcast_message)
        };

//********** Generic Notification ********************
        this.notify = function(broadcast_message) {
            $rootScope.$broadcast(broadcast_message);
        };
//*****************************************************

        this.show_loader = function (broadcast_message, loader_condition) {
            this.loader_condition = loader_condition;
            this.notify("show loader")
        };

        this.hide_loader = function (broadcast_message, loader_condition) {
            this.loader_condition = loader_condition;
            this.notify("hide loader")
        };

        this.show_alert = function (broadcast_message, alert_condition, alert_message) {
            this.alert_condition = alert_condition;
            this.alert_message = alert_message;
            this.notify("show alert")
        };

        this.hide_alert = function (broadcast_message, alert_condition) {
            this.alert_condition = alert_condition;
            this.notify("hide alert")
        };
        this.show_confirm = function (broadcast_message, alert_condition, confirm_message) {
            this.alert_condition = alert_condition;
            this.confirm_message = confirm_message;
            this.notify("show confirm")
        };
        this.confirm_true_false = function (broadcast_message, confirm_val) {
            this.confirm_val = confirm_val;
            this.notify(broadcast_message)
        };

    });
