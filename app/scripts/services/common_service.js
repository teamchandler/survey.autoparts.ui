/**
 * Created by Mahiruddin Seikh on 21-Oct-2016.
 */

'use strict';

angular.module('loyalty.survey.ui')
  .service('CommonService', function MasterService($rootScope, UtilService) {

    this.get_calls = function ($http, $q, data) {

      var apiPath = UtilService.get_login_api() + '/get/tab/content';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: apiPath,
        data: data,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    this.complete_retailer_call = function ($http, $q, data) {

      var apiPath = UtilService.get_login_api() + '/retailer/call/complete';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: apiPath,
        data: data,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    this.save_retailer_data = function ($http, $q, data) {

      var apiPath = UtilService.get_login_api() + '/save/retailer/details';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: apiPath,
        data: data,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    this.update_feedback = function ($http, $q, data) {

      var apiPath = UtilService.get_login_api() + '/retailer/survey/update';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: apiPath,
        data: data,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    this.get_all_exsisting_caller = function ($http, $q) {

      var apiPath = UtilService.get_login_api() + '/get/exsisting/caller/details';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: apiPath,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    this.insert_caller_credentials = function ($http, $q, credential) {

      var apiPath = UtilService.get_login_api() + '/caller/insert/details';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: apiPath,
        data: credential,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    this.get_wip_user_list = function ($http, $q) {

      var apiPath = UtilService.get_login_api() + '/get/wip/user/list';
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: apiPath,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    this.user_user_wip_data = function ($http, $q, data) {

      var apiPath = UtilService.get_login_api() + '/get/wip/user/data';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: apiPath,
        data: data,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };




    this.search_by_text_data = function ($http, $q, send_search_data) {

      var apiPath = UtilService.get_login_api() + '/get/data/by/search';
      var deferred = $q.defer();
      $http({
        method: 'POST',
        url: apiPath,
        data:send_search_data ,
        ContentType: 'application/json'
      }).success(function (data) {
        deferred.resolve(data);
      }).error(function (data) {
        deferred.reject("Data Error");
      });
      return deferred.promise;
    };

    //SHALINI

    //insert_super_adming_question

    this.insert_super_admin_question = function ($http,$q,insert_question)
    {
      var apiPath = UtilService.get_login_api()+'/insert/super/admin/question';
      var deferred = $q.defer();
      $http({
        method:'POST',
        url:apiPath,
        data:insert_question,
        ContentType:'application/json'
      }).success(function (data)
      {
        deferred.resolve(data) ;
      }).error(function (data)
      {
        deferred.reject("DATA ERROR");
      });
      return deferred.promise;

    }



    this.get_all_question_list = function ($http,$q)
    {
      var apiPath = UtilService.get_login_api()+'/get/question/display';
      var deferred = $q.defer();
      $http({
        method:'GET',
        url:apiPath,
        ContentType:'application/json'
      }).success(function (data)
      {
        deferred.resolve(data) ;
      }).error(function (data)
      {
        deferred.reject("DATA ERROR");
      });
      return deferred.promise;

    };




    this.get_retailer_details_data = function ($http,$q,retailer_code)
    {
      var apiPath = UtilService.get_login_api()+'/get/all/retailer/details';
      var deferred = $q.defer();
      $http({
        method:'POST',
        data:retailer_code,
        url:apiPath,
        ContentType:'application/json'
      }).success(function (data)
      {
        deferred.resolve(data) ;
      }).error(function (data)
      {
        deferred.reject("DATA ERROR");
      });
      return deferred.promise;

    };


    this.dont_save_data = function ($http,$q,retailer_code)
    {
      var apiPath = UtilService.get_login_api()+'/dont/instered/retailer';
      var deferred = $q.defer();
      $http({
        method:'POST',
        data:retailer_code,
        url:apiPath,
        ContentType:'application/json'
      }).success(function (data)
      {
        deferred.resolve(data) ;
      }).error(function (data)
      {
        deferred.reject("DATA ERROR");
      });
      return deferred.promise;

    };

  });
