'use strict';

angular.module('loyalty.survey.ui')
  .service('UtilService', function UtilService(localStorageService,
                                               NotificationService,$http) {

    // This service should be the ONLY interface to all config and localstorages
        this.get_reward_api = function (){
            return reward_api;
        };

        this.get_location_dependent_val = function (){
            return location_dependent;
        };

        this.get_program_name= function (){
            return program_name;
        };

        this.get_kyc_process= function (){
            return kyc_process;
        };

        this.get_default_title = function (){
            return default_title;
        };
        this.get_default_menu = function (){
            return default_menu;
        };
        this.get_menu = function (){
            return menu;
        };

        this.get_login_api = function () {
            return login_api;
        };

        this.get_api = function () {
            return api;
        };

        this.get_todo_api = function () {
            return todo_api;
        };
        this.get_es_api = function () {
            return es_api;
        };
        this.get_util_api = function () {
            return util_api;
        };

        this.get_sms_api = function () {
            return sms_api;
        };

        this.rooturl = function () {
            return rooturl;
        };

        this.get_company = function () {
            return company;
        };
        this.get_user_type = function () {
            return user_type;
        };

        this.get_encryption_phrase = function () {
            return encryption_phrase;
        };

        this.get_anenctos_program_admin = function () {
            return annectos_program_admin;
        };
        this.get_schneider_program_admin = function () {
            return schneider_program_admin;
        };

        this.get_state_list = function () {
            return state_list;
        };

        this.get_region_list = function () {//added By Mahiruddin Seikh for region list on date 08/07/2015
            return Region_list;
        };

        this.get_city_list = function () {
            return city_list;
        };

        this.get_default_role = function (){
            return default_role;
        }

        this.get_main_page_slider = function () {
            return adItems.landing_page_main_scroller;
        };

        this.get_program_start_date = function () {
            return prog_start_dt;
        };

        this.get_program_end_date = function () {
            return prog_end_dt;
        };

        this.get_to_email = function () {
            return to_email;
        };


        this.get_from_localstorage = function (key){
            return localStorageService.get(key);
        }
        this.set_to_localstorage = function (key, val){
            return localStorageService.add(key, val);
        }

        this.change_header = function(header_text){
            NotificationService.change_header("change header", (header_text));
        }
        this.select_menu = function(state_name){
            NotificationService.send_notification("state change", state_name);
        }
        this.send_notification = function(msg, data){
            NotificationService.send_notification(msg, data);
        }
//        this.load_config = function($http, $q){
//
//        }

        this.show_loader_login = function(){
            NotificationService.show_loader("show loader", true);
        };

        this.show_loader = function(){
            renewtoken();
            NotificationService.show_loader("show loader", true);
        };
        this.hide_loader = function(){
            NotificationService.hide_loader("hide loader", false);
        };

        this.get_from_localstorage = function (key){
            return localStorageService.get(key);
        }
        this.set_to_localstorage = function (key, val){
            return localStorageService.add(key, val);
        }




        this.access_chk = function ($http, $q, sendData) {
            var deferred = $q.defer();
            var apiPath = api + '/user/find_detail';
            var x ={view:false, add:false, edit:false, delete:false};
            $http({
                method: 'POST',
                headers: {token: localStorageService.get('token')},
                url: apiPath,
                data: sendData,
                ContentType: 'application/json'
            }).success(function (data) {

                try{
                if (data[0].access[0].access.view) {
                    x.view=true;

                }
                if (data[0].access[0].access.add) {
                    x.add=true;

                }
                if (data[0].access[0].access.edit) {
                    x.edit=true;

                }
                if (data[0].access[0].access.delete) {
                    x.delete=true;

                }
                    deferred.resolve(x);
                }


              catch(err){
                  deferred.resolve(x);
              }



            }).error(function (data) {
                deferred.resolve(x);


            })
            return deferred.promise;
        }

        function renewtoken() {
            if(localStorageService.get('token')&&localStorageService.get('token')!="") {
                var apiPath = api + '/renew/token';
                $http({
                    method: 'POST',
                    headers: {token: localStorageService.get('token')},
                    url: apiPath,
                    data: {},
                    ContentType: 'application/json'
                }).success(function (data) {
                    if(data=='Not authorised'){
                        alert("You May Not Access Or" +
                            "\nYour Login Has Expired !!");
                    }
                    else{
                    localStorageService.add("token", data);}
                }).error(function (data) {

                })
            }

        }

        this.rights_chk = function (menu_name) {
            var usr_grp=localStorageService.get("user_group_info");
            for(var i=0;i<usr_grp.rights.length;i++){
                if(usr_grp.rights[i].menu_name==menu_name){
                    if(usr_grp.rights[i].right_view==1){
                        return true
                    }
                    else{
                        return false
                    }
                }

            }
        }

        this.others_rights_chk = function (menu_name) {
            var x ={add:false, edit:false};
            var usr_grp=localStorageService.get("user_group_info");
            for(var i=0;i<usr_grp.rights.length;i++){
                if(usr_grp.rights[i].menu_name==menu_name){
                    if(usr_grp.rights[i].right_add==1){
                        x.add=true;
                    }
                    if(usr_grp.rights[i].right_edit==1){
                        x.edit=true;
                    }

                }

            }
            return x;
        }

        this.get_email_api = function () {
            return email_api;
        };
  });
