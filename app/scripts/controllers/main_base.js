/**
 * Created by Mahiruddin Seikh.
 */

'use strict';

angular.module('loyalty.survey.ui')
  .controller('MainBaseCtrl', function ($scope, $state, UtilService, localStorageService, $http, $q) {

    init();

    $scope.user_info = UtilService.get_from_localstorage("user_info");

    function check_login() {
      if (UtilService.get_from_localstorage("user_info") == null || UtilService.get_from_localstorage("user_info") == '') {
        $state.go('login');
      }
    }

    function init() {
      var user_info = localStorageService.get('user_info');
      $scope.user_data = UtilService.get_from_localstorage("user_info");
      $scope.user_group_data = UtilService.get_from_localstorage("user_group_info");

      check_login();
    }

    $scope.log_out = function () {
      UtilService.set_to_localstorage("user_info", []);
      UtilService.set_to_localstorage("distributor_list", []);
      UtilService.set_to_localstorage("token", []);
      $state.go('login');
    }

  });


