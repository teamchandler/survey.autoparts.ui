/**
 * Created by developer9 on 6/9/2015.
 */

'use strict';

angular.module('loyalty.survey.ui')
  .controller('alertctrl', function ($scope, $state,
                                     UtilService, NotificationService) {

    function init() {

      $scope.alert_msg = false;
    }

    $scope.$on('show alert', function () {
      $scope.alert_msg = NotificationService.alert_condition;
      $scope.alrt_message = NotificationService.alert_message;
    });

    $scope.$on('hide alert', function () {
      $scope.alert_msg = NotificationService.alert_condition;
      $scope.alrt_message = NotificationService.alert_message;
    });

    $scope.btn_ok = function () {
      $scope.alert_msg = false;

    }

    $scope.$on('show confirm', function () {
      $scope.confirm_msg = NotificationService.alert_condition;
      $scope.confirm_message = NotificationService.confirm_message;
    });


    $scope.btn_confirm_true = function () {
      $scope.confirm_msg = false;
      NotificationService.confirm_true_false('confirm true', true);
    }
    $scope.btn_confirm_false = function () {
      $scope.confirm_msg = false;
      NotificationService.confirm_true_false('confirm false', false);
    }

  });

