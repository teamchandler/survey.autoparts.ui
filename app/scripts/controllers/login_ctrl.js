/**
 * Created by Mahiruddin Seikh on 07-06-2016.
 */

'use strict';

angular.module('loyalty.survey.ui')
  .controller('LoginCtrl', function ($scope, $http, $q, $state, UtilService, LoginService, NotificationService, localStorageService) {

    init();

    function check_login() {
      if (UtilService.get_from_localstorage("user_info") == null || UtilService.get_from_localstorage("user_info") == '') {
        $state.go('login');
      }
    }

    //initialize method
    function init() {
      check_login();
      $scope.user_id;
      $scope.password;
      $scope.create_user=true;
    }

    $scope.login_user = function () {
      var login_data = {};
      login_data.user_id = $scope.user_id;
      login_data.password = $scope.password;
      LoginService.user_login($http, $q, login_data).then(function (data) {
        if (data.msg == "Log In Success") {
          // data_array.push(data);
          // console.log(data_array[0][0].user_detail);
          // NotificationService.show_alert('show alert',true,data.msg);
          UtilService.set_to_localstorage("user_info", data.user_deatil);
          UtilService.set_to_localstorage("token", data.token);
          var user_data = UtilService.get_from_localstorage("user_info");
          var user_type=user_data.user_type;

          if(user_type=="Super Tele Caller")
          {
            $state.go('main.create_login_caller_user');
            $scope.create_user=false;

          }
          else {
            $state.go('main.call_details');
            $scope.create_user=true;
          }

        }
        else {
          UtilService.hide_loader();
          NotificationService.show_alert('show alert', true, data.msg);
          UtilService.set_to_localstorage("user_info", "");
        }

      }),
        function () {
          $scope.error = error;

        }

    }




  });
