/**
 * Created by Mahiruddin Seikh on 07-06-2016.
 */

'use strict';

angular.module('loyalty.survey.ui')
  .controller('RetailerDetailsCtrl', function ($scope, $http, $q, $state, UtilService, CommonService, NotificationService, localStorageService) {

    function check_login() {
      if (UtilService.get_from_localstorage("user_info") == null || UtilService.get_from_localstorage("user_info") == '') {
        $state.go('login');
      }
    }

    init();

    //var stateParams = $state.retailer_code;
    $scope.address={};
    var user_data = UtilService.get_from_localstorage("user_info");
    var retailer_data = UtilService.get_from_localstorage("retailer_data");

    $scope.retailer_data = retailer_data;

    //console.log("stateParams : "+stateParams);
    //console.log("retailer_data : "+JSON.stringify(retailer_data));
    //initialize method


    function init() {

      check_login();
      //get_retailer_details();
      $scope.retailer_data;

      get_retailer_details();
    }

    $scope.save_retailer_data = function (retailer_data) {
      // console.log("retailer_data");
      // console.log(JSON.stringify(retailer_data));
      if($scope.address.address_confirmed==true)
      {
        $scope.retailer_data.address_confirmed="Yes";
      }
      else
      {
        $scope.retailer_data.address_confirmed="No";
      }
      if( $scope.retailer_data.call_status=="Do Not Want To Participate"||$scope.retailer_data.call_status=="Wrong Number"||$scope.retailer_data.call_status=="No Shop"||$scope.retailer_data.call_status=="Invalid Shop")
      {
        var user_data = UtilService.get_from_localstorage("user_info");
        $scope.retailer_data.user_id=user_data.user_id

        $scope.retailer_data.survey_auto_status="Complete";
        // $scope.retailer_data.call_status="Survey Done";
        CommonService.dont_save_data($http, $q, retailer_data)
          .then(
            function (data) {
              if (data.err) {
                // NotificationService.show_alert('show alert', true, data.err.err);
                // UtilService.hide_loader();
              } else {
                // console.log(data);
                NotificationService.show_alert('show alert', true, data.msg);
                // $state.go('main.feedback');
              }
            }
          );
      }
      else {
        var user_data = UtilService.get_from_localstorage("user_info");
        $scope.retailer_data.user_id=user_data.user_id

        CommonService.save_retailer_data($http, $q, retailer_data)
          .then(
            function (data) {
              if (data.err) {
                // NotificationService.show_alert('show alert', true, data.err.err);
                // UtilService.hide_loader();
              } else {
                // console.log(data);
                NotificationService.show_alert('show alert', true, data.msg);
                // $state.go('main.feedback');
              }
            }
          );
      }
      // $state.go('main.feedback');
    };

    function get_retailer_details() {
      UtilService.show_loader();
      var retailer_data = UtilService.get_from_localstorage("retailer_data");
      console.log("retailer_data" + JSON.stringify(retailer_data));
      var retailer_code = retailer_data.retailer_code
      console.log("retailer_code" + JSON.stringify(retailer_code))
      var object = {};
      object.retailer_code = retailer_code;
      console.log("object" + JSON.stringify(object))

      CommonService.get_retailer_details_data($http, $q, object).then(function (data) {

        UtilService.hide_loader();
        if (data.length > 0) {
          // $scope.address.address_confirmed=false;
          $scope.retailer_data = data[0];

        }
      })

    }


    $scope.check_status =function (call_status)
    {
      if(call_status=="Ready for Survey")
      {
        $state.go('main.feedback');
      }


    }
  });
