/**
 * Created by shalini on 11/11/2016.
 */

'use strict'


angular.module('loyalty.survey.ui').controller('CreateFeedBackCtrl', function ($scope, $http, $q, CommonService, UtilService,NotificationService) {

  $scope.super_admin = {};

  function init() {
    get_question_list();

  }

  //method to insert the question
  $scope.insert_desired_questions = function () {
    UtilService.show_loader();
    var user = UtilService.get_from_localstorage("user_info");
    var user_name = user.user_id;
    console.log("question" + $scope.super_admin);
    var insert_question = {};
    insert_question.question = $scope.super_admin.question;
    insert_question.rank = parseInt($scope.super_admin.rank);
    insert_question.created_by = user_name;
    insert_question.created_dt = new Date();
    insert_question.active = 1
    CommonService.insert_super_admin_question($http, $q, insert_question).then(function (data) {
      UtilService.hide_loader();
      if (data) {

        NotificationService.show_alert('show alert', true, data);
        get_question_list();
      }
      else {
        NotificationService.show_alert('show alert', true, "Question Not Created");
      }

    })

  };

  function get_question_list()
  {
    UtilService.show_loader();
    CommonService.get_all_question_list($http,$q).then(function (data)
    {
      UtilService.hide_loader();

if(data.length>0)
{
  $scope.questions_grid =data;
}
else
{
  NotificationService.show_alert('show alert','true',"Question Not Present Please Enter A New One!!!")
}
    })

  }
  init();

})
