'use strict';

/**
 * @ngdoc function
 * @name loyalty.survey.ui.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the loyalty.survey.ui
 */
angular.module('loyalty.survey.ui')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
