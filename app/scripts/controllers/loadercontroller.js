/**
 * Created by gghh on 9/19/2014.
 */


'use strict';

angular.module('loyalty.survey.ui')
    .controller('LoaderCtrl', function ($scope,$state,
                                        UtilService,  NotificationService) {

        function init(){

            $scope.loading=false;
        }



        $scope.$on('show loader', function() {
            $scope.loading = NotificationService.loader_condition;
        });

        $scope.$on('hide loader', function() {
            $scope.loading = NotificationService.loader_condition;
        });


    });

