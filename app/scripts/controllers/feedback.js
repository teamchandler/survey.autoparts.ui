
'use strict';

angular.module('loyalty.survey.ui')
  .controller('FeedBackCtrl', function ($scope, $http, $q, $state, UtilService, CommonService, NotificationService, localStorageService) {

    init();
    var movie_tickets_status = "No";

    $scope.survey_data = {};
    $scope.feeback = {};
    $scope.answer_list = {};

    function check_login() {
      if (UtilService.get_from_localstorage("user_info") == null || UtilService.get_from_localstorage("user_info") == '') {
        $state.go('login');
      }
    }

    var user_data = UtilService.get_from_localstorage("user_info");
    var retailer_data = UtilService.get_from_localstorage("retailer_data");
    var movie_tickets = "No";

    function init() {
      check_login();
      get_question();

    }

    /*$scope.update_feedback = function () {
     if ($scope.survey_data.movie_tickets_status == true) {
     $scope.survey_data.movie_tickets_status = "Yes";
     }
     else {
     $scope.survey_data.movie_tickets_status = movie_tickets;
     }

     if ($scope.survey_data.remarks == "" || $scope.survey_data.remarks == undefined || $scope.survey_data.remarks == '') {
     NotificationService.show_alert('show alert', true, 'Please Enter The Remarks');
     return false;
     }

     $scope.survey_data.retailer_code = retailer_data.retailer_code;
     $scope.survey_data.user_data = user_data;


     //console.log(JSON.stringify($scope.survey_data));

     CommonService.update_feedback($http, $q, $scope.survey_data)
     .then(
     function (data) {
     if (data.err) {
     // NotificationService.show_alert('show alert', true, data.err.err);
     // UtilService.hide_loader();
     } else {
     //console.log(data);
     alert(data.msg);

     $state.go('main.call_details');
     }
     }
     );

     };*/

    /*$scope.submit = function () {
     console.log($scope.retailer_data)
     if ($scope.retailer_data.answer1 == undefined || $scope.retailer_data.answer1 == "" || $scope.retailer_data.answer1 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer2 == undefined || $scope.retailer_data.answer2 == "" || $scope.retailer_data.answer2 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer3 == undefined || $scope.retailer_data.answer3 == "" || $scope.retailer_data.answer3 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer4 == undefined || $scope.retailer_data.answer4 == "" || $scope.retailer_data.answer4 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer5 == undefined || $scope.retailer_data.answer5 == "" || $scope.retailer_data.answer5 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer6 == undefined || $scope.retailer_data.answer6 == "" || $scope.retailer_data.answer6 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer7 == undefined || $scope.retailer_data.answer7 == "" || $scope.retailer_data.answer7 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer8 == undefined || $scope.retailer_data.answer8 == "" || $scope.retailer_data.answer8 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer9 == undefined || $scope.retailer_data.answer9 == "" || $scope.retailer_data.answer9 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer10 == undefined || $scope.retailer_data.answer10 == "" || $scope.retailer_data.answer10 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }
     if ($scope.retailer_data.answer11 == undefined || $scope.retailer_data.answer11 == "" || $scope.retailer_data.answer11 == '') {
     NotificationService.show_alert('show alert', true, "Please Fill All The Answers!!");
     }


     // $state.go('');

     }*/


    function get_question() {
      CommonService.get_all_question_list($http, $q).then(function (data) {
        var array = []
        if (data) {
          // console.log("data" + JSON.stringify(data));
          $scope.question_list = data;

        }

      })

    }

    $scope.send_answer = function () {
      if( $scope.survey_data.movie_tickets_status==true)
      {
        $scope.answer_list.movie_tickets_status="Yes";
      }
      else
      {
        $scope.answer_list.movie_tickets_status=movie_tickets;
      }


      // console.log("que" + JSON.stringify($scope.question_list));
      var question = $scope.question_list;
      // console.log("incmoing" + JSON.stringify($scope.answer_list))
      var data = [];
      var answer_object = {};
      answer_object = $scope.answer_list
      data.push($scope.answer_list);
      // console.log("ans_ar" + JSON.stringify(data.length));
      var answer1 = data[0];
      var j = 0;
      // console.log("answer1" + answer1);
      // var k = 0;
      var survey_data = [];
      var survey_data_array= [];
      for (var i = 0; i < question.length; i++) {
        var object = {};
        object.question = question[i].question;
        object.answer = data[0][i] || "";
        // console.log("object" + JSON.stringify(object));
        var object_c=survey_data.concat(object);
        // console.log("c"+JSON.stringify(object_c));
        survey_data_array.push(object);
        // console.log("survey_data_array"+JSON.stringify(survey_data_array));
      }


      // console.log("object1" + JSON.stringify(object));

      $scope.answer_list.retailer_code = retailer_data.retailer_code;
      $scope.answer_list.user_data = user_data;
      $scope.answer_list.survey = survey_data_array;
        $scope.answer_list.remarks=  $scope.survey_data.remarks
console.log("input"+ $scope.answer_list);
      CommonService.update_feedback($http, $q, $scope.answer_list)
        .then(
          function (data) {
            if (data.err) {
              // NotificationService.show_alert('show alert', true, data.err.err);
              // UtilService.hide_loader();
            } else {
              //console.log(data);
              alert(data.msg);

              $state.go('main.call_details');
            }
          }
        );

    }


  });
