/**
 * Created by Mahiruddin Seikh on 8/9/2016.
 */
'use strict';

angular.module('loyalty.survey.ui')
  .controller('CallDetailsCtrl', function ($scope, $http, $q, $state, UtilService, CommonService) {

    $scope.input_data = {};

    $scope.caller_list = [];

    $scope.global_call_type={};
        $scope.user={"user_id":""};

    var user_data = UtilService.get_from_localstorage("user_info");

    function check_login() {
      if (UtilService.get_from_localstorage("user_info") == null || UtilService.get_from_localstorage("user_info") == '') {
        $state.go('login');
      }
    }

    $scope.complete_retailer_call = function (retailer_data, input_data) {

      // console.log(retailer_data);
      // console.log(input_data);
      // console.log(user_data);

      var input_obj = {
        retailer_code: retailer_data.retailer_code,
        call_status: input_data.call_status,
        remarks: input_data.remarks,
        user_data: user_data
      };

      CommonService.complete_retailer_call($http, $q, input_obj)
        .then(
          function (data) {
            if (data.err) {
              // NotificationService.show_alert('show alert', true, data.err.err);
              // UtilService.hide_loader();
            } else {
              //console.log(data);

              $scope.get_callas('Pending');
              //$state.go('main.call_details');
              // alert(data.msg);
              // NotificationService.show_alert('show alert', true, data.msg);
              //$scope.calls_sata = data.data
            }
          }
        );
    };

    $scope.get_retailer_data = function (retailer_data) {
      $scope.retailer_data = retailer_data;
      $state.go('main.retailer_details');
      UtilService.set_to_localstorage("retailer_data", retailer_data);
    };

    $scope.wip_user_show = false;

    $scope.get_callas = function (call_type) {
      $scope.global_call_type=call_type;
      if (call_type == "WIP") {
        $scope.wip_user_show = true;

        $scope.caller_list = [];

        UtilService.show_loader();
        CommonService.get_wip_user_list($http, $q)
          .then(
            function (data) {
              if (data.err) {
                // NotificationService.show_alert('show alert', true, data.err.err);
                UtilService.hide_loader();
              } else {
                UtilService.hide_loader();
                // console.log(data);
                $scope.caller_list = data.data
              }
            }
          );

      }

      if (call_type == "Pending") {
        $scope.wip_user_show = false;
      }

      if (call_type == "Complete") {
        $scope.wip_user_show = false;
      }

      if (call_type == "Ignore") {
        $scope.wip_user_show = false;
      }

      UtilService.show_loader();

      $scope.pendclgrid.skip = 0;
      $scope.pendclgrid.limit = 50;

      var data = {
        call_status: call_type,
        call_type: "Retailer Loyalty Survey",
        "skip": $scope.pendclgrid.skip,
        "limit": $scope.pendclgrid.limit
      };

      CommonService.get_calls($http, $q, data)
        .then(
          function (data) {
            if (data.err) {
              // NotificationService.show_alert('show alert', true, data.err.err);
              UtilService.hide_loader();
            } else {
              UtilService.hide_loader();
              // console.log(data);
              $scope.calls_sata = data.data
            }
          }
        );
    };

    $scope.get_callas_append = function (call_type) {

      UtilService.show_loader();


      $scope.pendclgrid.skip = ($scope.pendclgrid.skip) * 1 + $scope.pendclgrid.limit;
      var call_type = "";
      var q = $scope.calls_sata.length - 1;
      var a = $scope.calls_sata[q].call_status;
      if (a == 'Survey Done') {
        call_type = 'Complete'
      }
      if (a == 'Fresh') {
        call_type = 'Pending'
      }
      if (a == 'RNR' | a == 'Call Back') {
        call_type = 'WIP'
      }
      if (a == 'Wrong Number' | a == 'No Shop' | a == 'Invalid Shop' | a == 'Do Not Want To Participate') {
        call_type = 'Ignore'
      }

      var data = {
        call_status: call_type, call_type: "Retailer Loyalty Survey",
        "skip": $scope.pendclgrid.skip,
        "limit": $scope.pendclgrid.limit
      };

      CommonService.get_calls($http, $q, data)
        .then(
          function (data) {
            if (data.err) {
              // NotificationService.show_alert('show alert', true, data.err.err);
              UtilService.hide_loader();
            } else {
              UtilService.hide_loader();
              // console.log(data);
              // $scope.calls_sata = data.data

              $scope.calls_sata = $scope.calls_sata.concat(data.data);
            }
          }
        );
    };

  /*  $scope.get_user_user_wip_data = function (data) {

      //alert(data.user_id);

      CommonService.user_user_wip_data($http, $q, "WIP", data.user_id)
        .then(
          function (data) {
            if (data.err) {
              // NotificationService.show_alert('show alert', true, data.err.err);
              UtilService.hide_loader();
            } else {
              UtilService.hide_loader();
              // console.log(data);
              $scope.calls_sata = data.data
            }
          }
        );

    };*/



        $scope.get_user_user_wip_data = function (data) {

            //alert(data.user_id);
            /*  if(call_type == "WIP"){
             $scope.wip_user_show = true;
             $scope.caller_list = [];*/
            $scope.user.user_id = data.user_id;
            UtilService.show_loader();
            CommonService.get_wip_user_list($http, $q)
                .then(
                function (data) {
                    if (data.err) {
                        // NotificationService.show_alert('show alert', true, data.err.err);
                        UtilService.hide_loader();
                    } else {
                        UtilService.hide_loader();
                        // console.log(data);
                        $scope.caller_list = data.data
                    }
                }
            );



            var data = {
                "user_id":  data.user_id, call_type: "WIP",
                "skip": $scope.pendclgrid.skip,
                "limit": $scope.pendclgrid.limit
            };
            CommonService.user_user_wip_data($http, $q, data)
                .then(
                function (data) {
                    if (data.err) {
                        // NotificationService.show_alert('show alert', true, data.err.err);
                        UtilService.hide_loader();
                    } else {
                        UtilService.hide_loader();
                        // console.log(data);
                        $scope.calls_sata = data.data
                    }
                }
            );

        };
        $scope.get_user_user_wip_data_append = function (data) {

            //alert(data.user_id);


            UtilService.show_loader();
            CommonService.get_wip_user_list($http, $q)
                .then(
                function (data) {
                    if (data.err) {
                        // NotificationService.show_alert('show alert', true, data.err.err);
                        UtilService.hide_loader();
                    } else {
                        UtilService.hide_loader();
                        // console.log(data);
                        $scope.caller_list = data.data
                    }
                }
            );


            var data = {
                "user_id":   $scope.user.user_id, call_type: "WIP",
                "skip": $scope.pendclgrid.skip,
                "limit": $scope.pendclgrid.limit
            };
            CommonService.user_user_wip_data($http, $q, data)
                .then(
                function (data) {
                    if (data.err) {
                        // NotificationService.show_alert('show alert', true, data.err.err);
                        UtilService.hide_loader();
                    } else {
                        UtilService.hide_loader();
                        // console.log(data);
                        $scope.calls_sata = data.data
                    }
                }
            );

        };



        function init() {

      check_login();

      var obj = {
        skip: 0,
        limit: 50,
        page_no: 1
      };

      $scope.pendclgrid = obj;
      $scope.get_callas('Pending');
    }

    jQuery("#pndinggridscroll").scroll(function () {
      if (jQuery("#pndinggridscroll").scrollTop() + jQuery("#pndinggridscroll").innerHeight() > this.scrollHeight - 1) {
          if($scope.user.user_id!="") {

              $scope.get_user_user_wip_data_append();
          }
          else
          {
              $scope.get_callas_append();
          }
      }
    });

    $scope.$watch('input_data.call_status', function (value) {
      if (value == 'Ready for Survey') {
        // console.log(value)
        $state.go('main.retailer_details');
      }
    });


    //shalini method for seach box
    $scope.search_by_data = function (search_data) {
      console.log($scope.global_call_type);
      UtilService.show_loader();
      // console.log("enter");
      // console.log("search" + search_data);

      var call_type=$scope.global_call_type;

      var send_search_data = {};
      send_search_data.regex_text = search_data;
      send_search_data.call_type=call_type;
      console.log("quer"+JSON.stringify(send_search_data));

      CommonService.search_by_text_data($http, $q, send_search_data).then(function (result_data) {
        UtilService.hide_loader();
        if (result_data) {
          console.log(result_data)
          $scope.calls_sata = result_data.data;
        }

      })
    }

    init();

  });
