/**
 * Created by Mahiruddin Seikh on 29-Oct-2016.
 */
'use strict';

angular.module('loyalty.survey.ui')
  .controller('CreateCallerCtrl', function ($scope, $state,
                                            UtilService, NotificationService, CommonService, $http, $q) {
    $scope.userinfo_data = {};

    function init() {

      $scope.userinfo_data = {};
      get_exsisting_caller_details();
      $scope.match_password=true;
      $scope.wrong_password=true;
    }

    init();

    $scope.user_info_data_save = function () {
      if ($scope.userinfo_data.user_id == undefined || $scope.userinfo_data.user_id == "" || $scope.userinfo_data.user_id == '') {
        NotificationService.show_alert('show alert', true, "Please Enter The User Id");
        return false;
      }

      if ($scope.userinfo_data.user_password == undefined || $scope.userinfo_data.user_password == "" || $scope.userinfo_data.user_password == '') {
        NotificationService.show_alert('show alert', true, "Please Enter The Password");
        return false;
      }

      if ($scope.userinfo_data.user_type == undefined || $scope.userinfo_data.user_type == "" || $scope.userinfo_data.user_type == '') {
        NotificationService.show_alert('show alert', true, "Please Select The User Type");
        return false;
      }
      $scope.userinfo_data.user_password;
      $scope.userinfo_data.confirm_user_password;
      var password =$scope.userinfo_data.user_password;
      var confirm_password= $scope.userinfo_data.confirm_user_password;

      if (password == confirm_password) {
        $scope.match_password = false;
        $scope.wrong_password=true;
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        var insert_caller_details = {};
        insert_caller_details.user_id = $scope.userinfo_data.user_id,
          insert_caller_details.user_password = $scope.userinfo_data.confirm_user_password,
          insert_caller_details.user_type = $scope.userinfo_data.user_type,
          insert_caller_details.user_first_name = $scope.userinfo_data.user_first_name,
          insert_caller_details.user_last_name = $scope.userinfo_data.user_last_name,
          insert_caller_details.user_middle_name = $scope.userinfo_data.user_middle_name,
          insert_caller_details.user_designation = $scope.userinfo_data.user_designation,
          insert_caller_details.user_email_id = $scope.userinfo_data.user_email_id,
          insert_caller_details.created_by = "admin",
          insert_caller_details.created_dt = day + "-" + month + "-" + year;
        UtilService.show_loader();
        CommonService.insert_caller_credentials($http, $q, insert_caller_details).then(function (data) {
          UtilService.hide_loader();
          NotificationService.show_alert('show alert', true, data);
          get_exsisting_caller_details();
        })

      }
      else {
        $scope.wrong_password=false;
        $scope.match_password = true;
      }


    }

    function get_exsisting_caller_details() {
      UtilService.show_loader();
      CommonService.get_all_exsisting_caller($http, $q).then(function (data) {
        UtilService.hide_loader();
        if (data) {
          $scope.userinfo_griddata = data.data;
        }
      })
    }

    $scope.pop_userinfo_data = function (usr) {
      console.log(usr)
      $scope.userinfo_data.user_id = usr.user_id;
      $scope.userinfo_data.user_password = usr.user_password;
      $scope.userinfo_data.user_first_name = usr.user_first_name;
      $scope.userinfo_data.user_last_name = usr.user_last_name;
      $scope.userinfo_data.user_middle_name = usr.user_middle_name;
      $scope.userinfo_data.user_designation = usr.user_designation;
      $scope.userinfo_data.user_email_id = usr.user_email_id;
      $scope.userinfo_data.user_id = usr.user_id;
    }
    $scope.clear_data = function () {
      $scope.userinfo_data = {};
    }
    /*  $scope.validate_confirm_password  = function ()
     {


     }*/


  });
